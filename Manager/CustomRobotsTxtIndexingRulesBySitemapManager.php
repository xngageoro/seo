<?php
#region copyright
/*
 * XNGAGE CONFIDENTIAL
 * __________________________
 *
 * Copyright (C) 2021 Xngage - All Rights Reserved
 *
 * All code or information contained herein is, and remains the
 * property of Xngage LLC and its customers.  The intellectual
 * and technical concepts contained are proprietary to Xngage LLC
 * and may be covered by U.S. and Foreign Patents, patents in
 * process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Xngage LLC.
 */
#endregion
namespace Xngage\Bundle\SeoBundle\Manager;

use Oro\Bundle\ConfigBundle\Config\ConfigManager;
use Oro\Bundle\SEOBundle\Manager\RobotsTxtFileManager;
use Oro\Bundle\SEOBundle\Manager\RobotsTxtIndexingRulesBySitemapManager;
use Oro\Bundle\SEOBundle\Sitemap\Provider\UrlItemsProviderRegistryInterface;
use Oro\Component\Website\WebsiteInterface;

class CustomRobotsTxtIndexingRulesBySitemapManager extends RobotsTxtIndexingRulesBySitemapManager
{
    private const ALLOW      = 'Allow';
    private const DISALLOW   = 'Disallow';
    private const USER_AGENT = 'User-Agent';

    private const AUTO_GENERATED_MARK = '# auto-generated';

    /** @var RobotsTxtFileManager */
    private $robotsTxtFileManager;

    /** @var ConfigManager */
    private $configManager;

    /** @var UrlItemsProviderRegistryInterface */
    private $urlItemsProviderRegistry;

    public function __construct(
        RobotsTxtFileManager $robotsTxtFileManager,
        ConfigManager $configManager,
        UrlItemsProviderRegistryInterface $urlItemsProviderRegistry
    ) {
        $this->robotsTxtFileManager = $robotsTxtFileManager;
        $this->configManager = $configManager;
        $this->urlItemsProviderRegistry = $urlItemsProviderRegistry;
    }

    /**
     * @param WebsiteInterface $website
     * @param string           $version
     */
    public function flush(WebsiteInterface $website, $version)
    {
        $content = $this->robotsTxtFileManager->getContent($website);
        $content = explode(PHP_EOL, $content);
        $regex = '/^\s*(%s|%s|%s)\s*:\s*(\/?.+)%s\s*$/i';
        $lineRegex = sprintf(
            $regex,
            self::ALLOW,
            self::DISALLOW,
            self::USER_AGENT,
            self::AUTO_GENERATED_MARK
        );
        foreach ($content as $key => $line) {
            if (preg_match($lineRegex, $line)) {
                unset($content[$key]);
            }
        }
        if (!$this->configManager->get('oro_frontend.guest_access_enabled', false, false, $website)) {
            $content[] = sprintf(
                '%s: * %s',
                self::USER_AGENT,
                self::AUTO_GENERATED_MARK
            );
            $providers = $this->urlItemsProviderRegistry->getProvidersIndexedByNames();

            $guestAllow = self::ALLOW;

            if ($this->configManager->get('xngage_seo.xngage_seo_config_disallow_all_urls_field')) {
                $guestAllow = self::DISALLOW;
            }

            foreach ($providers as $providerType => $provider) {
                foreach ($provider->getUrlItems($website, $version) as $urlItem) {
                    $allowUrl = parse_url($urlItem->getLocation(), PHP_URL_PATH);
                    $content[] = sprintf(
                        '%s: %s %s',
                        $guestAllow,
                        $allowUrl,
                        self::AUTO_GENERATED_MARK
                    );
                }
            }
            $content[] = sprintf(
                '%s: / %s',
                $guestAllow,
                self::AUTO_GENERATED_MARK
            );
        }

        $this->robotsTxtFileManager->dumpContent(implode(PHP_EOL, $content), $website);
    }
}