<?php
#region copyright
/*
 * XNGAGE CONFIDENTIAL
 * __________________________
 *
 * Copyright (C) 2021 Xngage - All Rights Reserved
 *
 * All code or information contained herein is, and remains the
 * property of Xngage LLC and its customers.  The intellectual
 * and technical concepts contained are proprietary to Xngage LLC
 * and may be covered by U.S. and Foreign Patents, patents in
 * process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Xngage LLC.
 */
#endregion
namespace Xngage\Bundle\SeoBundle\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class PublicTwigExtension extends AbstractExtension {

    public function getFilters()
    {
        return array(
            new TwigFilter('validUrl', array($this, 'validUrl')),
        );
    }

    public function validUrl($url)
    {
        return filter_var($url, FILTER_VALIDATE_URL);
    }

}