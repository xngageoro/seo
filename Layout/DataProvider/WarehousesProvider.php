<?php
#region copyright
/*
 * XNGAGE CONFIDENTIAL
 * __________________________
 *
 * Copyright (C) 2021 Xngage - All Rights Reserved
 *
 * All code or information contained herein is, and remains the
 * property of Xngage LLC and its customers.  The intellectual
 * and technical concepts contained are proprietary to Xngage LLC
 * and may be covered by U.S. and Foreign Patents, patents in
 * process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Xngage LLC.
 */
#endregion
namespace Xngage\Bundle\SeoBundle\Layout\DataProvider;

use Oro\Bundle\CustomerBundle\Entity\CustomerUser;
use Oro\Bundle\WarehouseBundle\SystemConfig\WarehouseConfigConverter;
use Oro\Bundle\SecurityBundle\Authentication\TokenAccessorInterface;
use Oro\Bundle\WarehouseBundle\Provider\WarehouseAddressProvider;
use Oro\Bundle\ConfigBundle\Config\ConfigManager;

/**
 * Provides enabled warehouses.
 */
class WarehousesProvider
{
    /**
     * @var ConfigManager
     */
    private $configManager;

    /**
     * @var WarehouseConfigConverter
     */
    private $warehouseConfigConverter;

    /**
     * @var TokenAccessorInterface
     */
    private $tokenAccessor;

    /**
     * @var WarehouseAddressProvider
     */
    private $warehouseAddressProvider;

    /**
     * @param ConfigManager $configManager
     * @param WarehouseConfigConverter $warehouseConfigConverter
     * @param TokenAccessorInterface $tokenAccessor
     */
    public function __construct(ConfigManager $configManager, WarehouseConfigConverter $warehouseConfigConverter, TokenAccessorInterface $tokenAccessor, WarehouseAddressProvider $warehouseAddressProvider)
    {
        $this->configManager = $configManager;
        $this->warehouseConfigConverter = $warehouseConfigConverter;
        $this->tokenAccessor = $tokenAccessor;
        $this->warehouseAddressProvider = $warehouseAddressProvider;
    }

    public function getUserOrDefaultWarehouse() {

        return [
            'street1' => '',
            'street2' => '',
            'phone' => '',
            'fax' => '',
            'email' => '',
            'sale_rep_name' => '',
            'sale_rep_email' => '',
            'country' => '',
            'city' => '',
            'state' => '',
            'zip' => '',
            'warehouse_name' => ''
          ];

        $warehouseResult = null;
        $currentUser = $this->tokenAccessor->getUser();

        if ($currentUser) {
            if ($currentUser instanceof CustomerUser) {
                $customer = $currentUser->getCustomer();

                if ($customer) {
                    $warehouse = $customer->getCustomDefaultWarehouse();

                    if ($warehouse) {
                        $warehouseAddress = $this->warehouseAddressProvider->getShippingOriginByWarehouse($warehouse);

                        $phone = $fax = '';

                        if ($warehouse->getCustomPhone()) {
                            $phone = sprintf("%s-%s-%s",
                                            substr($warehouse->getCustomPhone(), 0, 3),
                                            substr($warehouse->getCustomPhone(), 3, 3),
                                            substr($warehouse->getCustomPhone(), 6, 4));
                        }

                        if ($warehouse->getCustomPhone()) {
                            $fax = sprintf("%s-%s-%s",
                                substr($warehouse->getCustomFax(), 0, 3),
                                substr($warehouse->getCustomFax(), 3, 3),
                                substr($warehouse->getCustomFax(), 6, 4));
                        }

                        $warehouseResult = [
                            'street1' => $warehouseAddress->getStreet() ?? '',
                            'street2' => $warehouseAddress->getStreet2() ?? '',
                            'country' => $warehouseAddress->getCountryName(),
                            'city' => $warehouseAddress->getCity(),
                            'state' => $warehouseAddress->getRegionName(),
                            'zip' => $warehouseAddress->getPostalCode(),
                            'phone' => $phone ?? '',
                            'fax' => $fax ?? '',
                            'email' => $warehouse->getCustomEmail() ?? '',
                            'sale_rep_name' => $customer->getSalesRepName() ?? '',
                            'sale_rep_email' => $customer->getSalesRepEmail() ?? '',
                            'warehouse_name' => $warehouse->getName()
                        ];
                    }
                }
            }
        }
        if (empty($warehouseResult)) {
            $warehouseResult = [
              'street1' => $this->configManager->get('xngage_custom_features.corporate_address_street') ?? '',
              'street2' => $this->configManager->get('xngage_custom_features.corporate_address_street_2') ?? '',
              'phone' => $this->configManager->get('xngage_custom_features.corporate_address_phone') ?? '',
              'fax' => $this->configManager->get('xngage_custom_features.corporate_address_fax') ?? '',
              'email' => $this->configManager->get('xngage_custom_features.corporate_address_email') ?? '',
              'sale_rep_name' => $this->configManager->get('xngage_custom_features.sale_rep_name') ?? '',
              'sale_rep_email' => $this->configManager->get('xngage_custom_features.sale_rep_email') ?? '',
              'country' => '',
              'city' => '',
              'state' => '',
              'zip' => '',
              'warehouse_name' => ''
            ];
        }
        return $warehouseResult;
    }
}
