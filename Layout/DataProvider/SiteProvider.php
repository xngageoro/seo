<?php
#region copyright
/*
 * XNGAGE CONFIDENTIAL
 * __________________________
 *
 * Copyright (C) 2021 Xngage - All Rights Reserved
 *
 * All code or information contained herein is, and remains the
 * property of Xngage LLC and its customers.  The intellectual
 * and technical concepts contained are proprietary to Xngage LLC
 * and may be covered by U.S. and Foreign Patents, patents in
 * process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Xngage LLC.
 */
#endregion
namespace Xngage\Bundle\SeoBundle\Layout\DataProvider;

use Oro\Bundle\WebCatalogBundle\Provider\WebCatalogProvider;
use Oro\Bundle\WebsiteBundle\Manager\WebsiteManager;
use Oro\Bundle\AttachmentBundle\Manager\AttachmentManager;
use Doctrine\ORM\EntityManagerInterface;
use Oro\Bundle\AttachmentBundle\Entity\File;
use Oro\Bundle\WebCatalogBundle\Entity\ContentNode;
use Psr\Container\ContainerInterface;


class SiteProvider
{
    /**
     * @var WebCatalogProvider
     */
    private $webCatalogProvider;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var ConfigManager
     */
    private $configManager;

    /** @var ContainerInterface */
    private $container;

    /** @var WebsiteManager */
    private $websiteManager;

    /**
     * @param WebCatalogProvider $webCatalogProvider
     * @param ConfigManager $configManager
     * @param WebsiteManager $websiteManager
     */
    public function __construct(
        WebCatalogProvider $webCatalogProvider,
        ContainerInterface $container,
        EntityManagerInterface $entityManager,
        WebsiteManager $websiteManager,
        $configManager
    ) {
        $this->webCatalogProvider = $webCatalogProvider;
        $this->container = $container;
        $this->configManager =  $configManager;
        $this->entityManager =  $entityManager;
        $this->websiteManager = $websiteManager;
    }

    /**
     * @return AttachmentManager
     */
    protected function getAttachmentManager()
    {
        return $this->container->get(AttachmentManager::class);
    }

    public function getConfigParamValue($name)
    {
        return $this->configManager->get($name);
    }

    public function getSiteUrl()
    {
        $website = $this->websiteManager->getCurrentWebsite();
        $url = $this->configManager->get('oro_website.secure_url', false, false, $website);
        return $url ? $url : $this->getConfigParamValue('oro_ui.application_url');
    }

    public function getSiteName()
    {
        $currentWebCatalog = $this->webCatalogProvider->getWebCatalog();

        if ($currentWebCatalog) {
            $cn = $this->entityManager->getRepository(ContentNode::class)->getRootNodeByWebCatalog($currentWebCatalog);
            if (isset($cn->getTitles()[0])) {
                return $cn->getTitles()[0]->getString();
            }
        }

        return $this->getConfigParamValue('oro_ui.organization_name');
    }

    public function getLogo()
    {
        $file = $this->getConfigParamValue('xngage_custom_features.logo_field');
        if ($file == '') {
            return 'default';
        } else {
            $repo = $this->entityManager->getRepository(File::class);
            $fileObj = $repo->findOneById($file);
            
            return $this->getSiteUrl() . $this->getAttachmentManager()->getFileUrl($fileObj);
        }
    }
}
